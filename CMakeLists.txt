cmake_minimum_required(VERSION 2.6)
project(OpenGLHighPolyTest)

if (POLICY CMP0072)
  cmake_policy (SET CMP0072 NEW)
endif(POLICY CMP0072)

find_package(X11 REQUIRED)
find_package(GLEW REQUIRED)
find_package(OpenGL REQUIRED)
find_package(glfw3 REQUIRED)

add_executable(OpenGLHighPolyTest OpenGLHighPolyTest.cpp)

target_include_directories(OpenGLHighPolyTest PRIVATE ${GLEW_INCLUDE_DIRS} ${OPENGL_INCLUDE_DIRS})
target_link_libraries(OpenGLHighPolyTest PRIVATE ${X11_LIBRARIES} ${GLEW_LIBRARIES} ${OPENGL_LIBRARIES} glfw m)

include(FindPkgConfig)
pkg_search_module(OPENXR openxr)
if (OPENXR_FOUND)
  # uncomment to use an openxr/build directory that is next to the openxr-example directory
  #include_directories("${CMAKE_SOURCE_DIR}/../OpenXR-SDK-Source/build/include/")
  #link_directories("${CMAKE_SOURCE_DIR}/../OpenXR-SDK-Source/build/src/loader/")

  MESSAGE("OpenXR found with pkg-config")
  target_link_libraries(OpenGLHighPolyTest PRIVATE ${OPENXR_LIBRARIES})
else()
  MESSAGE("OpenXR not found with pkg-config, trying cmake script")
  list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
  find_package(OpenXR REQUIRED COMPONENTS headers loader)
  target_link_libraries(OpenGLHighPolyTest PRIVATE OpenXR::Loader)
endif()

if(MSVC)
  target_compile_options(OpenGLHighPolyTest PRIVATE /W4 /WX)
else(MSVC)
  target_compile_options(OpenGLHighPolyTest PRIVATE -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers)
endif(MSVC)

set_property(TARGET OpenGLHighPolyTest PROPERTY CXX_STANDARD 20)

install(TARGETS OpenGLHighPolyTest RUNTIME DESTINATION bin)
